package tsdv.tranning.batch27.group02.fsinspection.socketclient;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

import tsdv.tranning.batch27.group02.fsinspection.entity.CustomMessage;
import tsdv.tranning.batch27.group02.fsinspection.entity.FSfolder;
import tsdv.tranning.batch27.group02.fsinspection.model.CustomTreeTableModel;



public class FSsocketClient {
	
	private int port = 2222;
	private String ip = "127.0.0.1";
	private Socket socketClietn = null;  

	public FSsocketClient(){
		initConncetion(ip, port);
	}
	public FSsocketClient(String ip, int port){
		initConncetion(ip, port);
	}
	public void initConncetion(String ip, int port){
		try {
			socketClietn = new Socket(ip, port);
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	public void sendMessage(CustomMessage message){
		try {
			ObjectOutputStream out = new ObjectOutputStream(socketClietn.getOutputStream());
			out.writeObject(message);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public FSfolder getDir(){
		if(socketClietn != null && socketClietn.isConnected()){
			try {
				ObjectInputStream in  = new ObjectInputStream(socketClietn.getInputStream());
				FSfolder  folder = (FSfolder)in.readObject(); 
				return folder;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			//return null;
			} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
				e.printStackTrace();			
			}
		}
		return null;
	}
	public CustomTreeTableModel getTreeTableModel(){
		if(socketClietn != null && socketClietn.isConnected()){
			try {
				ObjectInputStream in  = new ObjectInputStream(socketClietn.getInputStream());
				CustomTreeTableModel  model = (CustomTreeTableModel)in.readObject(); 
				return model;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			//return null;
			} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
				e.printStackTrace();			
			}
		}
		return null;
	}
}
