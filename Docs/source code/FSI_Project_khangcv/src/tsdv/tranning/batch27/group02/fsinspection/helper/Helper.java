package tsdv.tranning.batch27.group02.fsinspection.helper;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;

import tsdv.tranning.batch27.group02.fsinspection.entity.FSfile;
import tsdv.tranning.batch27.group02.fsinspection.entity.FSfolder;

public class Helper {

	public static int getDeepLength(String path){
		String []patterns = path.split(":?\\\\");
		return patterns.length;
	}

	public static long calculateFolderSize(String path){
		long tmp = FileUtils.sizeOfDirectory(new File(path));
		//setSize(tmp);
		return tmp;
		
	}
	
	public static FSfolder getFolderDataByPath(String path){
		List<FSfolder> foldersList = new ArrayList<FSfolder>();
		List<FSfile> filesList = new ArrayList<FSfile>();
		File files[] = new File(path).listFiles();
		try {
			for (File file : files) {
				if(file.isFile())
					filesList.add(new FSfile(file.getPath()));
				else{
					foldersList.add(getFolderDataByPath(file.getPath()));
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
		FSfolder folder = new FSfolder(path);
		folder.setFiles(filesList);
		folder.setDirs(foldersList);
		return folder; 
	}
	public static FSfolder getFolderInFolderData(String folderPath, FSfolder data){
		if(data.getPath().equals(folderPath))
			return data;
		try {
			for (FSfolder folder : data.getDirs()) {
				getFolderInFolderData(folderPath, folder);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
