package tsdv.tranning.batch27.group02.fsinspection.model;

import java.util.HashMap;

public class FileInfor {

	private static HashMap<String, Long> fileInfor = new HashMap<String, Long>(); 
	//store information of file path and its size
	private static HashMap<String, String> parentInfor = new HashMap<String, String>();
	//store information of file path and its parent path
	
	public static void addFolderSize(String path, long size){
		fileInfor.put(path, size);
	}
	public static long getFolderSizeByPath(String path){
		return fileInfor.get(path);
	}
	
	public static void addParentInfor(String child, String parent){
		parentInfor.put(child, parent);
	}
	public static String getParentByPath(String path){
		return parentInfor.get(path);
	}

}
