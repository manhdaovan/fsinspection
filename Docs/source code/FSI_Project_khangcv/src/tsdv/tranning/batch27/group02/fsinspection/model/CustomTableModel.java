package tsdv.tranning.batch27.group02.fsinspection.model;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import tsdv.tranning.batch27.group02.fsinspection.entity.FSfile;
import tsdv.tranning.batch27.group02.fsinspection.entity.FSfolder;


public class CustomTableModel extends AbstractTableModel implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final String []COLUMN_NAME = {"File","Modified" ,"Checksum", "Permission"};
	private List<FSfile> data ;
	
	public CustomTableModel(String path){
		//data = new ArrayList<Filex>();
		File file = new File(path);
		if(file.isDirectory()){
			FSfolder tmp = new FSfolder(path);
			data = tmp.getFiles();
			System.out.println(data);
		}
	}
	@Override
	public int getColumnCount() {
		// TODO Auto-generated method stub
		return COLUMN_NAME.length;
	}
	

	@Override
	public String getColumnName(int column) {
		// TODO Auto-generated method stub
		return COLUMN_NAME[column];
	}


	@Override
	public int getRowCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getValueAt(int row, int col) {
		// TODO Auto-generated method stub
		FSfile row_data = data.get(row);
		
			switch (col) {
			case 0:
				return row_data.getPath();
			case 1:
				return row_data.lastModified();
			case 2:
				return 0;
			case 3:
				return 0;
				
			
			}
		
		
		return new String();
	}


	
	public List<FSfile> getData() {
		return data;
	}
	public void setData(List<FSfile> data) {
		this.data = data;
	}
	

}
