package tsdv.tranning.batch27.group02.fsinspection.entity;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class FSfolder extends File{

	private int noDirectories = -1;
	private int noFiles = -1;
	private int rootDeep;	// the deep of root folder
	private int currDeep;	// the deep of current folder
	private long size = -1;
	private Date modifiedDate = null;
	private List<FSfolder> dirs;
	private List<FSfile> files;
	private String percentage = null;
	
	public FSfolder(String pathname) {
		super(pathname);
		// TODO Auto-generated constructor stub
		this.files = new ArrayList<FSfile>();
		this.dirs = new ArrayList<FSfolder>();
		File files_tmp[] = this.listFiles();
		if(files_tmp != null){
			for (File file : files_tmp) {
				if(file.isFile())
					this.files.add(new FSfile(file.getPath()));
				else
					this.dirs.add(new FSfolder(file.getPath()));
				
			}
		}
		this.noDirectories = this.dirs.size();
		this.noFiles = this.files.size();
		
	}

	

	
	public boolean isDeepMax(){
		return (currDeep - rootDeep) > 10;
	}
	/*
	 * getters and setters*/
	public int getNoDirectories() {
		return noDirectories;
	}



	public void setNoDirectories(int noDirectories) {
		this.noDirectories = noDirectories;
	}



	public int getNoFiles() {
		return noFiles;
	}

	public void setNoFiles(int noFiles) {
		this.noFiles = noFiles;
	}

	public int getRootDeep() {
		return rootDeep;
	}

	public void setRootDeep(int rootDeep) {
		this.rootDeep = rootDeep;
	}

	public int getCurrDeep() {
		return currDeep;
	}

	public void setCurrDeep(int currDeep) {
		this.currDeep = currDeep;
	}

	public long getSize() {
		return size;
	}

	public void setSize(long size) {
		this.size = size;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public List<FSfolder> getDirs() {
		return dirs;
	}

	public void setDirs(List<FSfolder> dirs) {
		this.dirs = dirs;
	}

	public List<FSfile> getFiles() {
		return files;
	}

	public void setFiles(List<FSfile> files) {
		this.files = files;
	}

	public String getPercentage() {
		return percentage;
	}

	public void setPercentage(String percentage) {
		this.percentage = percentage;
	}



	
}
