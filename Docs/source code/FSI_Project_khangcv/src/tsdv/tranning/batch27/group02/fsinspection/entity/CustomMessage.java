package tsdv.tranning.batch27.group02.fsinspection.entity;

import java.io.Serializable;

public class CustomMessage implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int commmandIndex;
	private String filePath;
	
	public CustomMessage(int commmandIndex, String filePath) {
		super();
		this.commmandIndex = commmandIndex;
		this.filePath = filePath;
	}
	public int getCommmandIndex() {
		return commmandIndex;
	}
	public void setCommmandIndex(int commmandIndex) {
		this.commmandIndex = commmandIndex;
	}
	public String getFilePath() {
		return filePath;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	
}
