package tsdv.tranning.batch27.group02.fsinspection.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeExpansionListener;

import tsdv.tranning.batch27.group02.fsinspection.entity.CustomMessage;
import tsdv.tranning.batch27.group02.fsinspection.entity.FSfolder;
import tsdv.tranning.batch27.group02.fsinspection.helper.Helper;
import tsdv.tranning.batch27.group02.fsinspection.socketclient.FSsocketClient;
import tsdv.tranning.batch27.group02.fsinspection.view.MainWindow;
import tsdv.tranning.batch27.group02.fsinspection.view.OpenDialog;

public class MainController {
	private MainWindow mainWindow = null;
	private OpenDialog openDialog = null;
	private FSsocketClient socketClient = null;
	
	public MainController(MainWindow mainWindow){
		this.mainWindow = mainWindow;
		this.openDialog = new OpenDialog(mainWindow, true);
		addListenersForAll();
		
	}
	private void addListenersForAll(){
		addListenersForMainWindow();
		addListenersForOpenDialog();
	}
	private void addListenersForMainWindow(){
		//openMenuItem listener
		this.mainWindow.getOpenMenuItem().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				openMenuItemActionPerformed(arg0);
			}
		});
		//treeTable listeners
		this.mainWindow.getTreeTable().addTreeExpansionListener(new TreeExpansionListener() {
			
			@Override
			public void treeExpanded(TreeExpansionEvent arg0) {
				// TODO Auto-generated method stub
				treeTableTreeExpanded(arg0);
			}
			
			@Override
			public void treeCollapsed(TreeExpansionEvent arg0) {
				// TODO Auto-generated method stub
				
			}
		});
	}
	
	
	
	private void addListenersForOpenDialog(){
		this.openDialog.getOkButton().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				okButtonActionPerformed(e);				
			}
		});
		
	}
	/*
	 * Handle events from components on mainWindow*/
	private void openMenuItemActionPerformed(java.awt.event.ActionEvent evt) {                                             
        // TODO add your handling code here:
		if(openDialog == null){
			openDialog = new OpenDialog(mainWindow, true);
		}
		else
			openDialog.setVisible(true);		
		
		
    } 
	private void treeTableTreeExpanded(javax.swing.event.TreeExpansionEvent evt) {                                       
        // TODO add your handling code here:
		File selectedFile = (File)evt.getPath().getLastPathComponent();
		FSfolder data = this.mainWindow.getTreeTableModel().getData();
		this.mainWindow.getTableMode().getData().removeAll(this.mainWindow.getTableMode().getData());
		FSfolder folder = Helper.getFolderInFolderData(selectedFile.getPath(), data);
		this.mainWindow.getTableMode().setData(folder.getFiles());
    } 
	/*
	 * Handle events from components on OpenDialog*/
	private void okButtonActionPerformed(java.awt.event.ActionEvent evt) {                                         
        // TODO add your handling code here:
		String localDirectoryPath = System.getProperty("user.home");
		if(openDialog.getLocalRadioButton().isSelected()){
			this.openDialog.setVisible(false);
			JFileChooser fileChooser = new JFileChooser();
			fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			int result = fileChooser.showOpenDialog(this.mainWindow);
			if(result == JFileChooser.APPROVE_OPTION){
				File tmp = fileChooser.getSelectedFile(); 
				if(tmp != null)
					localDirectoryPath = tmp.getPath();				
			}
			mainWindow.dispose();
			mainWindow = new MainWindow(localDirectoryPath);
			addListenersForAll();
			
		}
		if(openDialog.getRemoteRadioButton().isSelected()){
			String ip = openDialog.getIpTextField().getText();
			String port = openDialog.getPortTextField().getText();
			if(ip != null && port != null){
				socketClient = new FSsocketClient(ip, Integer.parseInt(port));
				socketClient.sendMessage(new CustomMessage(1,System.getProperty("user.home")));
				socketClient.getDir();
				
			}
		}
    } 

}
