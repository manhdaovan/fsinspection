package tsdv.tranning.batch27.group02.fsinspection.model;

import java.io.File;
import java.io.Serializable;
import java.util.List;

import javax.swing.JOptionPane;

import org.jdesktop.swingx.treetable.AbstractTreeTableModel;

import tsdv.tranning.batch27.group02.fsinspection.entity.FSfile;
import tsdv.tranning.batch27.group02.fsinspection.entity.FSfolder;


public class CustomTreeTableModel extends AbstractTreeTableModel implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private FSfolder data;
	private final static String []COLUMN_NAME = {"Name","Size","%", "Dirs","Files"};
	
	public CustomTreeTableModel(FSfolder file){
		data = file;
	}
	@Override
	public String getColumnName(int column) {
		// TODO Auto-generated method stub
		return COLUMN_NAME[column];
	}


	


	@Override
	public Object getRoot() {
		// TODO Auto-generated method stub
		return data;
	}
	@Override
	public boolean isLeaf(Object node) {
		// TODO Auto-generated method stub
		//File tmp = (File)node;		
		return node instanceof FSfile;
	}


	@Override
	public int getColumnCount() {
		// TODO Auto-generated method stub
		return COLUMN_NAME.length;
	}

	@Override
	public Object getValueAt(Object node, int column) {
		// TODO Auto-generated method stub
		if(node instanceof FSfolder){
			FSfolder tmp = (FSfolder)node;
			switch (column) {
				case 0:
					return tmp.getName();
				case 1:
					//return 0;
					return tmp.getSize();
				case 2:
					return 0;
					//return String.format("%.2f", ((float)tmp.getSize()/(float)(Helper.getParent(root, tmp).getSize())));
				case 3:
					return tmp.getNoDirectories();
				case 4:
					return tmp.getNoFiles();
			
			}
		}
		if(node instanceof FSfile){
			FSfile tmp = (FSfile)node;
			switch (column) {
				case 0:
					return tmp.getName();
				case 1:
					return tmp.getParent();
				case 2:
					return 0;
				case 3:
					return 0;
			
			}
		}
		return null;
	}

	@Override
	public Object getChild(Object parent, int index) {
		// TODO Auto-generated method stub
		if(parent instanceof FSfolder){
			FSfolder directory = (FSfolder) parent;	
			if(! directory.isDeepMax())
				return directory.getDirs().get(index);
			else{
				new JOptionPane().showMessageDialog(null, "Access deny");
				return new FSfile(directory.getParent() + File.separator + "Access deny");
			}
		}
		//return null;
		if(data.getNoDirectories() > 100)
			return new FSfile(data.getPath() + File.separator + "Access deny");
		return getChild(data, index);
		
		 
	    
	}

	@Override
	public int getChildCount(Object parent) {
		// TODO Auto-generated method stub
		 /*File file = (File) parent;
		    if (file.isDirectory()) {
		      String[] fileList = file.list();
		      if (fileList != null)
		        return file.list().length;
		    }
		    return root.getChildren().size();*/
		if(parent instanceof FSfolder){
			FSfolder tmp = (FSfolder)parent;
			if(!tmp.isDeepMax())
				return tmp.getNoDirectories();
		}
		return getChildCount(data);
	}

	@Override
	public int getIndexOfChild(Object parent, Object child) {
		// TODO Auto-generated method stub
		if(parent instanceof FSfolder){
			FSfolder tmp = (FSfolder)parent;
			System.out.println(data.getPath());
			return tmp.getDirs().indexOf(child);
		}	    
	    return data.getDirs().indexOf(child);
	}
	public FSfolder getData() {
		return data;
	}
	public void setData(FSfolder data) {
		this.data = data;
	}
	
}
