/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tsdv.training.batch27.group02.fsinspection.util;

import java.io.Serializable;

/**
 *
 * @author manhdv
 */
public class FSCommandFormat implements Serializable{
    
    private int commandCode;
    private String data;
    
    public FSCommandFormat(int commandCode,String data){
        this.commandCode = commandCode;
        this.data = data;
    }
    
    public int getCommandCode(){
        return this.commandCode;
    }
    
    public String getData(){
        return this.data;
    }
}
