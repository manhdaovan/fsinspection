package tsdv.training.batch27.group02.fsinspection.util;

public class FSConstants {

    public final static String DEFAULT_IP = "127.0.0.1";
    public final static int DEFAULT_PORT = 6969;
    //some constant for main window
    public final static int DEFAULT_MAIN_WINDOW_WIDTH = 800;
    public final static int DEFAULT_MAIN_WINDOW_HEIGHT = 800;
    public final static int DEFAULT_MAIN_WINDOW_LEFT = 250;
    public final static int DEFAULT_MAIN_WINDOW_TOP = 100;
    public final static String DEFAULT_MAIN_WINDOW_TITLE = "FS Inspection";
    //some constant for option dialog
    public final static int DEFAULT_OPTION_DIALOG_WIDTH = 300;
    public final static int DEFAULT_OPTION_DIALOG_HEIGHT = 300;
    public final static int DEFAULT_OPTION_DIALOG_LEFT = 500;
    public final static int DEFAULT_OPTION_DIALOG_TOP = 300;
    public final static String DEFAULT_OPTION_DIALOG_TITLE = "Choose location to inspect";
    public final static String DEFAULT_ABOUT_DIALOG_TITLE = "About";
    public static final String PACKAGE_HOME_PATH = "src/tsdv/training/batch27/group02/fsinspection";
    //some constant for file detail 
    public static final String COLUMN_FILE = "File";
    public static final String COLUMN_SIZE = "Size";
    public static final String COLUMN_MODIFIED = "Modified";
    public static final String COLUMN_CHECKSUM = "Checksum";
    public static final String COLUMN_PERMISSION = "Permission";
    public static final String COLUMN_NO_PERMISSION = "?";
    
    public static final int MAXIMUM_THREAD = 10;

    public static interface CommandCode {

        public static final int GET_LIST_FOLDER = 1;
        public static final int GET_LIST_FILE = 2;
        public static final int DELETE_FILE = 3;
        public static final int GET_MD5_FILE = 4;
        public static final int GET_PERMISSION_FILE = 5;
        public static final int CHANGE_PERMISSION_FILE = 6;
    }

    public static interface ErrorCode {

        public static final int SUCCESS = 0;
        public static final int UNKNOW = 1;
        public static final int OVER_MAX_FOLDERS = 2;
        public static final int OVER_MAX_FILES = 3;
        public static final int CANNOT_CHANGE_PERMISSION = 4;
        public static final int FOLDER_NOT_EXISTED = 5;
        public static final int ERROR_TIMEOUT = 6;
        public static final int ERROR_UNABLE_CONNECT = 7;
    }

    public static interface TREE_FILE {

        public static final String COL_NAME = "Name";
        public static final String COL_SIZE = "Size";
        public static final String COL_PERCENT = "%";
        public static final String COL_DIR = "Dirs";
        public static final String COL_FILE = "Files";
    }

    public static final int COLUMN_PERCENTAGE = 2;
    public static final int COLUMN_FILENAME = 0;

    public static interface PERMISSION_DIALOG {

        public static final String COL_TYPE = "Type";
        public static final String COL_READ = "Read";
        public static final String COL_WRITE = "Write";
        public static final String COL_EXE = "Execute";

        public static final String ROW_OWNER = "Owner";
        public static final String ROW_GROUP = "Group";
        public static final String ROW_OTHER = "Other";

        public static final String BTN_SAVE = "Save";
        public static final String BTN_CANCEL = "Cancel";

        public static final String PER_READ = "r";
        public static final String PER_WRITE = "w";
        public static final String PER_EXECUTE = "x";
        public static final String PER_TITLE = "Change Permission";
    }

    public static interface EXIT_DIALOG {

        public static final String EXIT_CONTENT = "Do you want to exit this application?";
        public static final String EXIT_TITLE = "Exit";
    }

    public static interface ABOUT_DIALOG {

        public static final String ABOUT_CONTENT = "<html><left><b><font size=+2>FSInspection tool</font></b><br>"
                + "<br>Tool is developped and run on Ubuntu."
                + "<br>Author: Training group 2 - TSDV."
                + "<br>Version: 1.0"
                + "<br>(C) Copyright 2014.";
        public static final String ABOUT_TITLE = "About";
        public static final String ABOUT_BTN_OK = "OK";
    }

    public static interface MessageContent {

        public static final String MESSAGE_CANNOT_READ = "You do not have permisisons neccessary to view the contents of\n";
        public static final String MESSAGE_FILE_TOO_LARGE = "FS Tool does not support files with size larger than 500MB!\n";
        public static final String MESSAGE_DIR_TOO_MANY_FILES = "FS Tool does not support directory that has more than 100 files!\n";
        public static final String MESSAGE_DIR_TOO_MANY_SUBDIRS = "FS Tool does not support directory that has more than 10 sub-directories!\n";
        public static final String MESSAGE_GENERAL_ERROR = "Something wrong here";
    }

    public static interface EVENT_ID {

        public static final int FILELIST_TABLE_FOCUS_GAINED = 1;
        public static final int FILELIST_TABLE_FOCUS_LOST = 2;
        public static final int FILELIST_TABLE_SELECTED = 3;
    }

    public static final String MD5 = "md5";
    public static final String BUTTON_ENABLE = "button_enable";
    public static final String BUTTON_DISABLE = "button_disable";

    public static final int MAX_SIZE_MD5 = 500 * 1024 * 1024; // 500MB in byte
    public static final int MAX_DIRS = 10;
    public static final int MAX_FILES = 100;

    public static final String PREF_LAST_LOCATION = "FileChooserLastLocation";
}
