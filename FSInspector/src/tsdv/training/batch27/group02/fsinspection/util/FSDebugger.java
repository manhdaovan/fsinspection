package tsdv.training.batch27.group02.fsinspection.util;

public class FSDebugger {
	public static enum DebugLevel {
		NOT_INITIALIZED(-1), NONE(0), INFO(1), DEBUG(2), MAX_LEVEL(3);
		
		private int value;
		
		DebugLevel(int val)	{
			value = val;
		}
		
		int getValue()	{
			return value;
		}
	};

	private DebugLevel mLevel;
	
	private static FSDebugger mDebugger = null;
	
	FSDebugger(DebugLevel level)	{
		setLevel(level);
	}
	
	public static FSDebugger getInstance()	{
		if (mDebugger == null)	{
			mDebugger = new FSDebugger(DebugLevel.NONE);
		}
		
		return mDebugger;
	}
	
	public void setLevel(DebugLevel level)	{
		mLevel = level;		
	}

	public DebugLevel getLevel()	{
		return mLevel;
	}
	
	public void printLog(DebugLevel level, String format, Object... args) {
		
		if (getLevel().getValue() >= level.getValue())	{
			System.out.format("\n"+format, args);
		}
	}
}
