/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tsdv.training.batch27.group02.fsinspection.util;

import java.io.Serializable;

/**
 *
 * @author manhdv
 */
public class FSResult implements Serializable{
    
    public int error;
    public Object[] data;
    
    public FSResult(int error, Object[] data){
        this.error = error;
        this.data = data;
    }
    
    public FSResult(){
        this.error = FSConstants.ErrorCode.UNKNOW;
        this.data = null;
    }

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }

    public Object[] getData() {
        return data;
    }

    public void setData(Object[] data) {
        this.data = data;
    }
    
    
    
}
