/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tsdv.training.batch27.group02.fsinspection.util;

import java.io.File;

/**
 *
 * @author manhdv
 */
public class FSFolder extends File {
    
    private String name;
    private long size;
    private String percentagePerTotal;
    private int numberSubfiles;
    private int numberSubdirs;
    
    public FSFolder(String path) {
        super(path);
        if (this.exists() && this.isDirectory()) {
            caculateFolderSize(new File(path));
            percentagePerTotal = "100";
            for (File file : this.listFiles()) {
                if (file.isFile()) {
                    numberSubfiles++;
                } else if (file.isDirectory()) {
                    numberSubdirs++;
                }
            }
        }
    }
    
    public FSFolder(String path, long parentSize) {
        super(path);
        if (this.exists() && this.isDirectory()) {
            caculateFolderSize(new File(path));
            percentagePerTotal = String.format("%.2f", (float) size / (float) parentSize);
            for (File file : this.listFiles()) {
                if (file.isFile()) {
                    numberSubfiles++;
                } else if (file.isDirectory()) {
                    numberSubdirs++;
                }
            }
        }
    }
    
    private long caculateFolderSize(File folder) {
        if (folder.exists()) {
            for (File file : folder.listFiles()) {
                if (file.isFile()) {
                    size += file.length();
                } else {
                    size += caculateFolderSize(file);
                }
            }
        }
        return size;
    }
    
    public long getSize() {
        return size;
    }
    
    public void setSize(long size) {
        this.size = size;
    }
    
    public String getPercentagePerTotal() {
        return percentagePerTotal;
    }
    
    public void setPercentagePerTotal(String percentagePerTotal) {
        this.percentagePerTotal = percentagePerTotal;
    }
    
    public int getNumberSubfiles() {
        return numberSubfiles;
    }
    
    public void setNumberSubfiles(int numberSubfiles) {
        this.numberSubfiles = numberSubfiles;
    }
    
    public int getNumberSubdirs() {
        return numberSubdirs;
    }
    
    public void setNumberSubdirs(int numberSubdirs) {
        this.numberSubdirs = numberSubdirs;
    }
    
}
