package tsdv.training.batch27.group02.fsinspection.util;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.UserPrincipal;
import java.util.ArrayList;
import java.util.Arrays;

import org.apache.commons.io.FileUtils;

public class FileSystemHelpers {
	public static final FilenameFilter DIR_FILTER = new FilenameFilter() {

		public boolean accept(File current, String name) {
			return new File(current, name).isDirectory();
		}
	};

	public static final FilenameFilter FILE_FILTER = new FilenameFilter() {

		public boolean accept(File current, String name) {
			return new File(current, name).isFile();
		}
	};

	/**
	 * Check if this file is a file (include symlink) or a directory
	 * 
	 * @param file
	 * @return
	 */
	public static boolean isFile(File file) {
		try {
			// do not use isFile(); some system files return false
			return ((((File) file).list() == null) || FileUtils.isSymlink(file));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * Get the number of sub-files in directory parent
	 * 
	 * @param parent
	 * @return
	 */
	public static int getNumberOfSubFiles(File parent) {
		int count = 0;
		if (!parent.exists()) {
			String msg = parent + " does not exist";
			throw new IllegalArgumentException(msg);
		}
		if (!parent.isDirectory()) {
			String msg = parent + " is not a directory";
			throw new IllegalArgumentException(msg);
		}

		File[] listFile = parent.listFiles();
		if (listFile != null && listFile.length > 0) {
			for (int i = 0; i < listFile.length; i++) {
				if (listFile[i].isFile()) {
					count += 1;
				}else if (listFile[i].isDirectory()) {
					try {
						if (FileUtils.isSymlink(listFile[i])) {
							count += 1;
						}
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
		return count;
	}

	/**
	 * Check if current user have right to alter this file's permission Only
	 * root and owner of the file can change its permission
	 * 
	 * @param file
	 * @return
	 */
	public static boolean isPermissionModifiable(File file) {
		try {
			String currentUser = System.getProperty("user.name");
			if (currentUser.equals("root")) {
				return true;
			}
			UserPrincipal owner = Files.getOwner(
					Paths.get(file.getAbsolutePath()),
					new LinkOption[] { LinkOption.NOFOLLOW_LINKS });
			if (currentUser.equals(owner.getName())) {
				return true;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * Calculate size of file (do not follow link if it is a symbolic link)
	 * 
	 * @param file
	 * @return
	 */
	public static long getSizeOfFilesNoFollowLink(File file) {
		try {
			BasicFileAttributes attr = Files.readAttributes(
					Paths.get(file.getAbsolutePath()),
					BasicFileAttributes.class, LinkOption.NOFOLLOW_LINKS);
			return attr.size();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return 0;
	}

	/**
	 * Get size of a directory
	 * 
	 * @param directory
	 * @return
	 */
	public static long getSizeOfDirectory(File directory) {
		if (!directory.exists()) {
			String message = directory + " does not exist";
			throw new IllegalArgumentException(message);
		}

		if (!directory.isDirectory()) {
			String message = directory + " is not a directory";
			throw new IllegalArgumentException(message);
		}

		long size = 0;
		File[] files = directory.listFiles();
		if (files == null) { // null if security restricted
			return 0L;
		}
		for (int i = 0; i < files.length; i++) {
			File file = files[i];
			if (file.exists() && file.isDirectory()) {
				try {
					if (FileUtils.isSymlink(file)) {
						// Do not follow symlink
						size += FileSystemHelpers
								.getSizeOfFilesNoFollowLink(file);
					} else {
						size += FileSystemHelpers.getSizeOfDirectory(file);
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else {
				size += FileSystemHelpers.getSizeOfFilesNoFollowLink(file);
			}
		}
		return size;
	}
	
	/**
	 * Get total size of all files (include symlink) of a directory
	 * 
	 * @param directory
	 * @return
	 */
	public static long getSizeOfChildFiles(File directory) {
		if (!directory.exists()) {
			String message = directory + " does not exist";
			throw new IllegalArgumentException(message);
		}

		if (!directory.isDirectory()) {
			String message = directory + " is not a directory";
			throw new IllegalArgumentException(message);
		}

		long size = 0;
		File[] files = directory.listFiles();
		if (files == null) { // null if security restricted
			return 0L;
		}
		for (int i = 0; i < files.length; i++) {
			File file = files[i];
			if (file.exists() && file.isDirectory()) {
				try {
					if (FileUtils.isSymlink(file)) {
						// Do not follow symlink
						size += FileSystemHelpers
								.getSizeOfFilesNoFollowLink(file);
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else {
				size += FileSystemHelpers.getSizeOfFilesNoFollowLink(file);
			}
		}
		return size;
	}

	/**
	 * List all file and symlink in a directory
	 * 
	 * @param directory
	 * @return
	 */
	public static File[] listFileAndSymlinkInDirectory(File directory) {
		File[] originalFile = directory
				.listFiles(FileSystemHelpers.FILE_FILTER);
		ArrayList<File> fileList = new ArrayList<File>(
				Arrays.asList(originalFile));
		File[] dirList = directory.listFiles(FileSystemHelpers.DIR_FILTER);

		if (dirList != null && dirList.length > 0) {
			for (int i = 0; i < dirList.length; i++) {
				try {
					if (FileUtils.isSymlink(dirList[i])) {
						fileList.add(dirList[i]);
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return fileList.toArray(new File[fileList.size()]);
	}

	/**
	 * List all sub-directories (exclude symlink) in a directory
	 * 
	 * @param directory
	 * @return
	 */
	public static File[] listSubDirectory(File directory) {		
		File[] originalDir = directory.listFiles(FileSystemHelpers.DIR_FILTER);
		ArrayList<File> fileList = new ArrayList<File>(
				Arrays.asList(originalDir));

		if (originalDir != null && originalDir.length > 0) {
			for (int i = 0; i < originalDir.length; i++) {
				try {
					if (FileUtils.isSymlink(originalDir[i])) {
						fileList.remove(originalDir[i]);
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return fileList.toArray(new File[fileList.size()]);
	}
	
	/**
	 * Format size into human-readable format
	 * @param bytes
	 * @param si
	 * @return
	 */
	public static String humanReadableByteCount(long bytes, boolean si) {
		int unit = si ? 1000 : 1024;
		if (bytes < unit)
			return bytes + " B";
		int exp = (int) (Math.log(bytes) / Math.log(unit));
		String pre = (si ? "kMGTPE" : "KMGTPE").charAt(exp - 1)
				+ (si ? "" : "i");
		return String.format("%.1f %sB", bytes / Math.pow(unit, exp),
				pre.toUpperCase());
	}
}
