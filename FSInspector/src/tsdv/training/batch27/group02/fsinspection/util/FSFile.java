/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tsdv.training.batch27.group02.fsinspection.util;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.attribute.PosixFilePermission;
import java.nio.file.attribute.PosixFilePermissions;
import java.util.Date;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author manhdv
 */
public class FSFile extends File {

    private String name;
    private long size;
    private float percentagePerTotal;
    private String permission;
    private String modified;

    public FSFile(String path, long parentSize) {
        super(path);
        if (this.exists() && this.isFile()) {
            try {
                this.permission = "";
                this.name = this.getName();
                this.size = this.length();
                this.percentagePerTotal = (float) this.size / parentSize;
                Set<PosixFilePermission> set = Files.getPosixFilePermissions(Paths.get(this.getPath()));
                this.permission = PosixFilePermissions.toString(set);
                this.fullPath = path + this.name;
                this.modified = new Date(this.lastModified()).toString();
                
            } catch (IOException ex) {
                Logger.getLogger(FSFile.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public FSFile(String name, long size, float percent){
        super(".");
        this.name = name;
        this.size = size;
        this.percentagePerTotal = percent;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public float getPercentagePerTotal() {
        return percentagePerTotal;
    }

    public void setPercentagePerTotal(float percentagePerTotal) {
        this.percentagePerTotal = percentagePerTotal;
    }

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }

    public String getFullPath() {
        return fullPath;
    }

    public void setFullPath(String fullPath) {
        this.fullPath = fullPath;
    }
    private String fullPath;

    public String getFileName() {
        return this.name;
    }

    public long getFileSize() {
        return this.size;
    }

    public String getModified() {
        return modified;
    }

    public void setModified(String modified) {
        this.modified = modified;
    }
    
    public String convertSizeToHuman(boolean si){
        int unit = si ? 1000 : 1024;
        if (size < unit)
                return size + " B";
        int exp = (int) (Math.log(size) / Math.log(unit));
        String pre = (si ? "kMGTPE" : "KMGTPE").charAt(exp - 1)
                        + (si ? "" : "i");
        return String.format("%.1f %sB", size / Math.pow(unit, exp),
                        pre.toUpperCase());
        
    }
}
