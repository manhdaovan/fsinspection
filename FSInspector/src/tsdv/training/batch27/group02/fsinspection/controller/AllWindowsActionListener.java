/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tsdv.training.batch27.group02.fsinspection.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import tsdv.training.batch27.group02.fsinspection.client.FSSocketClient;
import tsdv.training.batch27.group02.fsinspection.util.*;
import tsdv.training.batch27.group02.fsinspection.view.AboutWindow;
import tsdv.training.batch27.group02.fsinspection.view.OptionWindow;
import tsdv.training.batch27.group02.fsinspection.view.MainWindow;

/**
 *
 * @author manhdv
 */
public class AllWindowsActionListener implements ActionListener {

    private MainWindow mainWindow;
    private OptionWindow optionWindow;
    private JFileChooser fileChooser;
    
    private ActionListener listenerOpenMenu;
    private ActionListener listenerDisconnectMenu;
    private ActionListener listenerAboutMenu;
    private ActionListener listenerOkButtonOnOptionDialog;
    private ActionListener listenerPermissionButton;
    private ActionListener listenerMd5Button;
    private ActionListener listenerDeleteButton;

    private FSSocketClient clientSocket;

    public AllWindowsActionListener() {
        mainWindow = new MainWindow();
        mainWindow.enableDeleteButton(false);
        mainWindow.enableMd5Button(false);
        mainWindow.enablePermissionButton(false);
        optionWindow = new OptionWindow(mainWindow, true);
        fileChooser = new JFileChooser(new File("."));
        
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void registerAllActionListener() {
        //Handle Open menu select
        listenerOpenMenu = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                displayOptionDialog();
            }
        };
        mainWindow.getjMenuItemOpen().addActionListener(listenerOpenMenu);

        //Handle Disconnect menu select
        listenerDisconnectMenu = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                disconnect();
            }
        };
        mainWindow.getjMenuItemDisconnect().addActionListener(listenerDisconnectMenu);

        //Handle About menu select
        listenerAboutMenu = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                displayAboutWindow();
            }
        };
        mainWindow.getjMenuItemAbout().addActionListener(listenerAboutMenu);

        //Handle Ok button on OptionWindow
        listenerOkButtonOnOptionDialog = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                displayFileChooser();
            }
        };
        optionWindow.getjButtonOk().addActionListener(listenerOkButtonOnOptionDialog);

        //Handle Permisson button click
        listenerPermissionButton = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                changePermission(new Object());
            }
        };
        mainWindow.getjButtonPermission().addActionListener(listenerPermissionButton);
        
        //Handle Md5 button click
        listenerMd5Button = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                caculateMd5(new Object());
            }
        };
        mainWindow.getjButtonMD5().addActionListener(listenerMd5Button);

        //Handle Delete button click
        listenerDeleteButton = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                deleteFile(new Object());
            }
        };
        mainWindow.getjButtonDelete().addActionListener(listenerDeleteButton);

        //display MainWindow to operate
        displayMainWindow();
    }
    
    private void displayFileChooser(){
        hideOptionWindow();
        if (clientSocket == null)
            clientSocket = new FSSocketClient();
        
        fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        int returnVal = fileChooser.showOpenDialog(mainWindow);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
                File file = fileChooser.getSelectedFile();
            try {
                FSResult result = clientSocket.getDataFromServer(FSConstants.CommandCode.GET_LIST_FOLDER, file.getCanonicalPath());
            } catch (Exception ex) {
                showAlertPopup(FSConstants.MessageContent.MESSAGE_GENERAL_ERROR);
            }
        }else{
            showOptionWindow();
        }
    }
    private void hideOptionWindow(){
        optionWindow.setVisible(false);
    }
    
    private void showOptionWindow(){
        optionWindow.setVisible(true);
    }
    
    private void showAlertPopup(String message) {
        JOptionPane.showMessageDialog(mainWindow, message);
    }

    private int showConfirmPopup(String message, String title) {
        return JOptionPane.showConfirmDialog(mainWindow, message, title, JOptionPane.YES_OPTION);
    }

    private boolean deleteFile(Object somethingHere) {
        if (showConfirmPopup(new String("Message"), new String("Title")) == JOptionPane.YES_OPTION) {
            //Delete File
            return true;
        }
        return false;
    }

    private void caculateMd5(Object somethingHere) {

    }

    private void changePermission(Object somethingHere) {

    }

    private void enableDisconnectMenu(boolean cond) {
        mainWindow.getjMenuItemDisconnect().setEnabled(cond);
    }

    private void displayAboutWindow() {
        final AboutWindow aboutWindow = new AboutWindow(mainWindow, true);
        aboutWindow.open();
    }

    private void disconnect() {
        if (clientSocket != null) {
            clientSocket.disconnect();
            clientSocket = null;
        }
    }

    private void displayOptionDialog() {
        optionWindow.open();
    }

    public void displayMainWindow() {
        mainWindow.open();
        enableDisconnectMenu(false);
    }
}
