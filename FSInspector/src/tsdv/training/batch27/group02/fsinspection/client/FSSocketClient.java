/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tsdv.training.batch27.group02.fsinspection.client;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import tsdv.training.batch27.group02.fsinspection.util.*;

/**
 *
 * @author manhdv
 */
public class FSSocketClient {

    private Socket client;

    public enum CommandCode {

        GET_LIST_FILE, GET_LIST_SUBDIRECTORY,
        GET_NUMBER_FILES, GET_NUMBER_DIRECTORIES
    }

    public enum ErrorCode {

        REQUEST_TIMEOUT
    }

    public String serverIp;
    public int serverPort;

    public FSSocketClient() {
        this.serverIp = FSConstants.DEFAULT_IP;
        this.serverPort = FSConstants.DEFAULT_PORT;
        try {
            client = new Socket(serverIp, serverPort);
        } catch (IOException ex) {
            Logger.getLogger(FSSocketClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public FSSocketClient(String serverIp, int serverPort) {
        this.serverIp = serverIp;
        this.serverPort = serverPort;
        try {
            client = new Socket(serverIp, serverPort);
        } catch (IOException ex) {
            Logger.getLogger(FSSocketClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void disconnect() {
        try {
            if (client != null) {
                this.client.close();
            }
        } catch (IOException ex) {
//            Logger.getLogger(FSSocketClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public FSResult getDataFromServer(int commandCode, String params) {
        FSCommandFormat command = new FSCommandFormat(commandCode, params);
        try {
            if (client == null || client.isClosed()) {
                client = new Socket(serverIp, serverPort);
            }
            ObjectOutputStream out = new ObjectOutputStream(client.getOutputStream());
            out.writeObject(command);
            ObjectInputStream in = new ObjectInputStream(client.getInputStream());
            return (FSResult) in.readObject();
        } catch (Exception ex) {
            return new FSResult(FSConstants.ErrorCode.UNKNOW, null);
        }
    }

}
