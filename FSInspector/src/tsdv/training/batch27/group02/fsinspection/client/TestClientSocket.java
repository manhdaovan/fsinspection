/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tsdv.training.batch27.group02.fsinspection.client;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import tsdv.training.batch27.group02.fsinspection.util.FSConstants;
import tsdv.training.batch27.group02.fsinspection.util.FSCommandFormat;
import tsdv.training.batch27.group02.fsinspection.util.FSResult;

/**
 *
 * @author manhdv
 */
public class TestClientSocket {

    public final static String DEFAULT_IP = "127.0.0.1";
    public final static int DEFAULT_PORT = 6969;

    public static void main(String[] args) {
        try {
            //init connection
            Socket client = new Socket(DEFAULT_IP, DEFAULT_PORT);

            //init IO
            System.err.println("Init IO");
            ObjectInputStream inputStream = new ObjectInputStream(client.getInputStream());
            System.err.println("Init IO 2");
            ObjectOutputStream outputStream = new ObjectOutputStream(client.getOutputStream());
            
            //*
            //send data to server
            System.err.println("Pre-send data to server");
            FSCommandFormat command = new FSCommandFormat(FSConstants.CommandCode.GET_LIST_FOLDER, "/var/lib/");
            outputStream.writeObject(command);
            System.err.println("Send data to server");

            //receive result
            FSResult result = null;
            try {
                result = (FSResult) inputStream.readObject();
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(TestClientSocket.class.getName()).log(Level.SEVERE, null, ex);
            }
            System.err.println("Error code: " + result.error);
            //*/
            outputStream.close();
            inputStream.close();
        } catch (IOException ex) {
            Logger.getLogger(TestClientSocket.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
