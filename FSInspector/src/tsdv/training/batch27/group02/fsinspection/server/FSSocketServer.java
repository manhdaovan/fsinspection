/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tsdv.training.batch27.group02.fsinspection.server;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import tsdv.training.batch27.group02.fsinspection.util.*;

/**
 *
 * @author manhdv
 */
public class FSSocketServer {

    public static final int DEFAULT_PORT = 6969;
    public static final int MAX_CLIENT = 10;
    private int numberClient;

    public FSSocketServer() {
        this.numberClient = 0;
    }

    public static void main(String[] args) {
        int port = (args.length == 1) ? Integer.parseInt(args[0]) : DEFAULT_PORT;
        FSResult result = new FSResult(FSConstants.ErrorCode.UNKNOW, null);
        try {
            //init server
            ServerSocket server = new ServerSocket(port);
            System.err.println("Pre-accept connection");

            //accept connection
            Socket client = server.accept();
            System.err.println("Accept client from " + client.getInetAddress());

            //init IO
            ObjectInputStream inputStream = new ObjectInputStream(client.getInputStream());
            System.err.println("Init input done");
            ObjectOutputStream outputStream = new ObjectOutputStream(client.getOutputStream());
            System.err.println("Init output done");

            //*
            try {
                //receive data from client
                FSCommandFormat command = (FSCommandFormat) inputStream.readObject();
                int commandCode = command.getCommandCode();
                System.err.println("Command code " + commandCode);
                switch (commandCode) {
                    case FSConstants.CommandCode.GET_LIST_FOLDER:
                        result = getListDirectory(command);
                        break;
                    case FSConstants.CommandCode.DELETE_FILE:
                        result = deleteFile(command);
                        break;
                    default:
                        break;
                }
                outputStream.writeObject(result);
            } catch (ClassNotFoundException ex) {
                outputStream.writeObject(result);
            }
            //*/
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private static FSResult getListDirectory(FSCommandFormat command) {
        FSResult result = new FSResult(FSConstants.ErrorCode.FOLDER_NOT_EXISTED, null);
        try {
            if (command.getCommandCode() == FSConstants.CommandCode.GET_LIST_FOLDER) {
                String path = command.getData();
                FSFolder folder = new FSFolder(path);
                if (folder.getNumberSubdirs() > FSConstants.MAX_DIRS){
                    result.error = FSConstants.ErrorCode.OVER_MAX_FOLDERS;
                }
                if (folder.getNumberSubfiles() > FSConstants.MAX_FILES){
                    result.error = FSConstants.ErrorCode.OVER_MAX_FILES; 
                }
            }
        } catch (Exception ex) {
            return result;
        }
        return result;
    }

    private static FSResult deleteFile(FSCommandFormat command) {
        return new FSResult(FSConstants.ErrorCode.UNKNOW, null);
    }
}
