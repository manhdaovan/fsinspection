/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tsdv.training.batch27.group02.fsinspection.model;

import tsdv.training.batch27.group02.fsinspection.util.FSFolder;
import org.jdesktop.swingx.treetable.AbstractTreeTableModel;
import org.jdesktop.swingx.treetable.TreeTableModel;

/**
 *
 * @author manhdv
 */
public class FileSystemModel extends AbstractTreeTableModel {

    static protected String[] cNames = {"Name", "Size", "%", "Dirs", "Files"};
    static protected Class[] cTypes = {TreeTableModel.class, String.class, Float.class, Integer.class, Integer.class};

    public FileSystemModel(FSFolder root) {
        super(root);
    }
    public FileSystemModel(){
        this.root = null;
    }
    public void setRoot(FSFolder root){
        this.root = root;
    }
    @Override
    public int getColumnCount() {
        return cNames.length;
    }

    @Override
    public Object getValueAt(Object o, int i) {
        return null;
    }

    @Override
    public Object getChild(Object parent, int index) {
        return null;
    }

    @Override
    public int getChildCount(Object parent) {
        return 0;
    }

    @Override
    public int getIndexOfChild(Object parent, Object child) {
        return 0;
    }

    @Override
    public String getColumnName(int column) {
        return cNames[column];
    }

    @Override
    public Class getColumnClass(int column) {
        return cTypes[column];
    }
}
