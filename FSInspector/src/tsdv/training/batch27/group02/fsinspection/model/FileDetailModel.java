/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tsdv.training.batch27.group02.fsinspection.model;

import java.util.Date;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;
import tsdv.training.batch27.group02.fsinspection.util.FSFile;
import tsdv.training.batch27.group02.fsinspection.util.FSFolder;

/**
 *
 * @author manhdv
 */
public class FileDetailModel extends AbstractTableModel {

    static protected String[] cNames = {"Name", "Size", "Modified", "Checksum", "Permission"};
    static protected Class[] cTypes = {String.class, String.class, String.class, String.class, String.class};

    FSFolder folder;

    public FileDetailModel(FSFolder folder) {
        this.folder = folder;
    }

    public FileDetailModel() {
        this.folder = null;
    }

    @Override
    public int getRowCount() {
        return (folder == null) ? 0 : folder.getNumberSubfiles();
    }

    @Override
    public int getColumnCount() {
        return cNames.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        return new Object();
    }

}
